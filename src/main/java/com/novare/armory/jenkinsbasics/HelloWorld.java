package com.novare.armory.jenkinsbasics;

import java.util.logging.Logger;

/**
 * A simple Hello World demonstration.
 * <p>
 * This simply ensures that we have a working Java project and that our build
 * runner can properly see through its classes.
 *
 * @author jerieljan
 */
public class HelloWorld {

    public static void main(String[] args) {
        //This is a demonstration, so we're just using a simple logger for now.
        //On a proper application, please use a logging framework instead of
        //java.util.logging.Logger.
        Logger logger = Logger.getLogger("com.novare.armory.jenkinsbasics.HelloWorld");

        //Creating a box.
        logger.info("Creating a 10x10 box.");
        Box testBox = new Box(10, 10);

        //Showing the output of said box.
        logger.info("The box's height is: " + testBox.getHeight());
        logger.info("The box's width is: " + testBox.getWidth());
    }
}
